Format: 3.0 (quilt)
Source: gambas3
Binary: gambas3,gambas3-dev-tools,gambas3-gb-args,gambas3-gb-cairo,gambas3-gb-chart,gambas3-gb-clipper,gambas3-gb-clipper2,gambas3-gb-complex,gambas3-gb-compress,gambas3-gb-compress-bzlib2,gambas3-gb-compress-zlib,gambas3-gb-compress-zstd,gambas3-gb-crypt,gambas3-gb-data,gambas3-gb-db,gambas3-gb-db-form,gambas3-gb-db-mysql,gambas3-gb-db-odbc,gambas3-gb-db-postgresql,gambas3-gb-db-sqlite3,gambas3-gb-db2,gambas3-gb-db2-form,gambas3-gb-db2-mysql,gambas3-gb-db2-odbc,gambas3-gb-db2-postgresql,gambas3-gb-db2-sqlite3,gambas3-gb-dbus,gambas3-gb-dbus-trayicon,gambas3-gb-desktop,gambas3-gb-desktop-x11,gambas3-gb-eval-highlight,gambas3-gb-form,gambas3-gb-form-dialog,gambas3-gb-form-editor,gambas3-gb-form-htmlview,gambas3-gb-form-mdi,gambas3-gb-form-print,gambas3-gb-form-stock,gambas3-gb-form-terminal,gambas3-gb-gmp,gambas3-gb-gsl,gambas3-gb-gtk,gambas3-gb-gtk-opengl,gambas3-gb-gtk3,gambas3-gb-gtk3-opengl,gambas3-gb-gtk3-webview,gambas3-gb-highlight,gambas3-gb-httpd,gambas3-gb-image,gambas3-gb-image-effect,gambas3-gb-image-imlib,gambas3-gb-image-io,gambas3-gb-inotify,gambas3-gb-libxml,gambas3-gb-logging,gambas3-gb-map,gambas3-gb-markdown,gambas3-gb-media,gambas3-gb-media-form,gambas3-gb-memcached,gambas3-gb-mime,gambas3-gb-mongodb,gambas3-gb-mysql,gambas3-gb-ncurses,gambas3-gb-net,gambas3-gb-net-curl,gambas3-gb-net-pop3,gambas3-gb-net-smtp,gambas3-gb-openal,gambas3-gb-opengl,gambas3-gb-opengl-glsl,gambas3-gb-opengl-glu,gambas3-gb-opengl-sge,gambas3-gb-openssl,gambas3-gb-option,gambas3-gb-pcre,gambas3-gb-pdf,gambas3-gb-poppler,gambas3-gb-qt5,gambas3-gb-qt5-ext,gambas3-gb-qt5-opengl,gambas3-gb-qt5-webkit,gambas3-gb-qt5-webview,gambas3-gb-report,gambas3-gb-report2,gambas3-gb-scanner,gambas3-gb-sdl,gambas3-gb-sdl-sound,gambas3-gb-sdl2,gambas3-gb-sdl2-audio,gambas3-gb-settings,gambas3-gb-signal,gambas3-gb-term,gambas3-gb-term-form,gambas3-gb-util,gambas3-gb-util-web,gambas3-gb-v4l,gambas3-gb-vb,gambas3-gb-web,gambas3-gb-web-feed,gambas3-gb-web-form,gambas3-gb-web-gui,gambas3-gb-xml,gambas3-gb-xml-html,gambas3-gb-xml-rpc,gambas3-gb-xml-xslt,gambas3-gui,gambas3-gui-opengl,gambas3-gui-webview,gambas3-ide,gambas3-runtime,gambas3-scripter
Architecture: any all
Version: 0.0.1
Maintainer: Willy Raets <gbWilly@protonmail.com>
Uploaders: Willy Raets <gbWilly@protonmail.com>
Homepage: https://gambas.sourceforge.net
Standards-Version: 4.5.0
Vcs-Browser: https://gitlab.com/gambas/gambas
Vcs-Git: https://gitlab.com/gambas/gambas.git
Build-Depends:  automake,
                autoconf,
                build-essential,
                debhelper-compat (=12),
                dh-autoreconf,
                gcc-11 [armhf], g++-11 [armhf],
                gettext,
                git,
                libalure-dev,
                libasound2-dev,
                libavcodec58,
                libavformat58,
                libbz2-dev,
                libcairo2-dev,
                libcurl4-gnutls-dev,
                libdirectfb-dev,
                libdumb1-dev,
                libffi-dev,
                libgdk-pixbuf2.0-dev,
                libglew-dev,
                libglib2.0-dev,
                libgmime-3.0-dev,
                libgmp-dev,
                libgsl-dev,
                libgstreamer-plugins-base1.0-dev,
                libgstreamer1.0-dev,
                libgtk-3-dev,
                libgtk2.0-dev,
                libgtkglext1-dev,
                libimlib2-dev,
                libjack-dev,
                libjpeg-dev,
                libmongoc-dev,
                libmysqlclient-dev,
                libncurses5-dev,
                libpcre3-dev,
                libpoppler-cpp-dev,
                libpoppler-dev,
                libpoppler-glib-dev,
                libpoppler-private-dev,
                libpq-dev,
                libqt5opengl5-dev,
                libqt5svg5-dev,
                libqt5webkit5-dev,
                libqt5x11extras5-dev,
                librsvg2-dev,
                libsdl1.2-dev,
                libsdl-image1.2-dev,
                libsdl-mixer1.2-dev,
                libsdl-sound1.2-dev,
                libsdl-ttf2.0-dev,
                libsdl2-dev,
                libsdl2-image-dev,
                libsdl2-mixer-dev,
                libsdl2-ttf-dev,
                libsqlite3-dev,
                libssl-dev,
                libunwind-dev,
                libv4l-dev,
                libwebkit2gtk-4.0-dev,
                libxml2-dev,
                libxslt1-dev,
                libxtst-dev,
                libzstd-dev,
                linux-libc-dev,
                qtbase5-dev,
                qtwebengine5-dev [amd64 arm64 armhf i386],
                sane-utils,
                unixodbc-dev,
                xdg-utils
Package-list:
 gambas3 deb devel optional arch=all
 gambas3-dev-tools deb devel optional arch=any
 gambas3-gui deb libdevel optional arch=all
 gambas3-gui-opengl deb libdevel optional arch=all
 gambas3-gui-webview deb libdevel optional arch=all
 gambas3-ide deb devel optional arch=all
 gambas3-runtime deb libdevel optional arch=any
 gambas3-scripter deb libdevel optional arch=all
 gambas3-gb-desktop deb libdevel optional arch=all
 gambas3-gb-scanner deb libdevel optional arch=all
 gambas3-gb-gtk3 deb libdevel optional arch=any
 gambas3-gb-qt5 deb libdevel optional arch=any
 gambas3-gb-args deb libdevel optional arch=all
 gambas3-gb-cairo deb libdevel optional arch=any
 gambas3-gb-chart deb libdevel optional arch=all
 gambas3-gb-clipper deb libdevel optional arch=any
 gambas3-gb-clipper2 deb libdevel optional arch=any
 gambas3-gb-complex deb libdevel optional arch=any
 gambas3-gb-compress deb libdevel optional arch=any
 gambas3-gb-compress-bzlib2 deb libdevel optional arch=any
 gambas3-gb-compress-zlib deb libdevel optional arch=any
 gambas3-gb-compress-zstd deb libdevel optional arch=any
 gambas3-gb-crypt deb libdevel optional arch=any
 gambas3-gb-data deb libdevel optional arch=any
 gambas3-gb-db deb libdevel optional arch=any
 gambas3-gb-db-form deb libdevel optional arch=all
 gambas3-gb-db-mysql deb libdevel optional arch=any
 gambas3-gb-db-odbc deb libdevel optional arch=any
 gambas3-gb-db-postgresql deb libdevel optional arch=any
 gambas3-gb-db-sqlite3 deb libdevel optional arch=any
 gambas3-gb-db2 deb libdevel optional arch=all
 gambas3-gb-db2-form deb libdevel optional arch=all
 gambas3-gb-db2-mysql deb libdevel optional arch=any
 gambas3-gb-db2-odbc deb libdevel optional arch=any
 gambas3-gb-db2-postgresql deb libdevel optional arch=any
 gambas3-gb-db2-sqlite3 deb libdevel optional arch=any
 gambas3-gb-dbus deb libdevel optional arch=any
 gambas3-gb-dbus-trayicon deb libdevel optional arch=all
 gambas3-gb-desktop-x11 deb libdevel optional arch=any
 gambas3-gb-eval-highlight deb libdevel optional arch=all
 gambas3-gb-form deb libdevel optional arch=all
 gambas3-gb-form-dialog deb libdevel optional arch=all
 gambas3-gb-form-editor deb libdevel optional arch=all
 gambas3-gb-form-htmlview deb libdevel optional arch=any
 gambas3-gb-form-mdi deb libdevel optional arch=all
 gambas3-gb-form-print deb libdevel optional arch=all
 gambas3-gb-form-stock deb libdevel optional arch=all
 gambas3-gb-form-terminal deb libdevel optional arch=all
 gambas3-gb-gmp deb libdevel optional arch=any
 gambas3-gb-gsl deb libdevel optional arch=any
 gambas3-gb-gtk deb libdevel optional arch=any
 gambas3-gb-gtk-opengl deb libdevel optional arch=any
 gambas3-gb-gtk3-opengl deb libdevel optional arch=any
 gambas3-gb-gtk3-webview deb libdevel optional arch=any
 gambas3-gb-highlight deb libdevel optional arch=all
 gambas3-gb-httpd deb libdevel optional arch=any
 gambas3-gb-image deb libdevel optional arch=any
 gambas3-gb-image-effect deb libdevel optional arch=any
 gambas3-gb-image-imlib deb libdevel optional arch=any
 gambas3-gb-image-io deb libdevel optional arch=any
 gambas3-gb-inotify deb libdevel optional arch=any
 gambas3-gb-libxml deb libdevel optional arch=any
 gambas3-gb-logging deb libdevel optional arch=all
 gambas3-gb-map deb libdevel optional arch=all
 gambas3-gb-markdown deb libdevel optional arch=all
 gambas3-gb-media deb libdevel optional arch=any
 gambas3-gb-media-form deb libdevel optional arch=all
 gambas3-gb-memcached deb libdevel optional arch=all
 gambas3-gb-mime deb libdevel optional arch=any
 gambas3-gb-mongodb deb libdevel optional arch=any
 gambas3-gb-mysql deb libdevel optional arch=all
 gambas3-gb-ncurses deb libdevel optional arch=any
 gambas3-gb-net deb libdevel optional arch=any
 gambas3-gb-net-curl deb libdevel optional arch=any
 gambas3-gb-net-pop3 deb libdevel optional arch=all
 gambas3-gb-net-smtp deb libdevel optional arch=all
 gambas3-gb-openal deb libdevel optional arch=any
 gambas3-gb-opengl deb libdevel optional arch=any
 gambas3-gb-opengl-glsl deb libdevel optional arch=any
 gambas3-gb-opengl-glu deb libdevel optional arch=any
 gambas3-gb-opengl-sge deb libdevel optional arch=any
 gambas3-gb-openssl deb libdevel optional arch=any
 gambas3-gb-option deb libdevel optional arch=any
 gambas3-gb-pcre deb libdevel optional arch=any
 gambas3-gb-pdf deb libdevel optional arch=any
 gambas3-gb-poppler deb libdevel optional arch=any
 gambas3-gb-qt5-ext deb libdevel optional arch=any
 gambas3-gb-qt5-opengl deb libdevel optional arch=any
 gambas3-gb-qt5-webkit deb libdevel optional arch=any
 gambas3-gb-qt5-webview deb libdevel optional arch=any
 gambas3-gb-report deb libdevel optional arch=all
 gambas3-gb-report2 deb libdevel optional arch=all
 gambas3-gb-sdl deb libdevel optional arch=any
 gambas3-gb-sdl-sound deb libdevel optional arch=any
 gambas3-gb-sdl2 deb libdevel optional arch=any
 gambas3-gb-sdl2-audio deb libdevel optional arch=any
 gambas3-gb-settings deb libdevel optional arch=all
 gambas3-gb-signal deb libdevel optional arch=any
 gambas3-gb-term deb libdevel optional arch=any
 gambas3-gb-term-form deb libdevel optional arch=all
 gambas3-gb-util deb libdevel optional arch=all
 gambas3-gb-util-web deb libdevel optional arch=any
 gambas3-gb-v4l deb libdevel optional arch=any
 gambas3-gb-vb deb libdevel optional arch=any
 gambas3-gb-web deb libdevel optional arch=all
 gambas3-gb-web-feed deb libdevel optional arch=all
 gambas3-gb-web-form deb libdevel optional arch=all
 gambas3-gb-web-gui deb libdevel optional arch=all
 gambas3-gb-xml deb libdevel optional arch=any
 gambas3-gb-xml-html deb libdevel optional arch=any
 gambas3-gb-xml-rpc deb libdevel optional arch=all
 gambas3-gb-xml-xslt deb libdevel optional arch=any
Files:
 437a8d1df895203af8b08e64717a6e30 2893892 gambas3-3.20.99+git.20250308.orig.tar.bz2
 0323ebdd4a1983de61a5e4f326194426 20710 gambas3-3.20.99+git.20250308.debian.tar.gz
DEBTRANSFORM-TAR: gambas-@VERSION@.tar.gz
DEBTRANSFORM-FILES-TAR: debian-xUbuntu_22.04.tar.gz
