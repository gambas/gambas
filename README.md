<!--<a href="http://gambaswiki.org/wiki/screenshot/cygwin-ide.png?v"><img src="http://gambaswiki.org/wiki/screenshot/t-cygwin-ide.png?v"></a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="http://gambaswiki.org/wiki/screenshot/webform-ide.png?v"><img src="http://gambaswiki.org/wiki/screenshot/t-webform-ide.png?v"></a>&nbsp;&nbsp;&nbsp;&nbsp;-->

![The Gambas development environment](https://gambaswiki.org/wiki/screenshot/2025-01-10.png)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Gambas Almost Means BASIC

WELCOME TO GAMBAS!

GAMBAS is a free implementation of a graphical development environment 
based on a BASIC interpreter and a full development platform. It is very 
inspired by Visual Basic and Java.

Go to http://gambas.sourceforge.net to get more information: how to compile 
and install it, where to find binary packages, how to report a bug...

Go to http://gambaswiki.org for language documentation.

The following pieces of code were borrowed and adapted:

- The natural string comparison algorithme was adapted from the algorithm 
  made by Martin Pol. See http://sourcefrog.net/projects/natsort/ for more 
  details.

- The hash table implementation was adapted from the glib one.

- The HTML entities parsing in 'gb.gtk' comes from KHTML sources.

- The 'gb.image.effect' sources are adapted from KDE 3 image effect routines.

- The 'gb.clipper' library embeds the Clipper library. See
  http://www.angusj.com/delphi/clipper.php for mode details.

- The function that computes the easter day of a specific year uses an
  algorithm made by Aloysius Lilius And Christophorus Clavius.

- The blurring algoritm is based on the 'StackBlur' algorithm made by Mario 
  Klingemann. See http://incubator.quasimondo.com/processing/fast_blur_deluxe.php 
  for more details.
  
- The javascript automatic completion is done with 'autoComplete' from Simon 
  Steinberger / Pixabay, and is published under the MIT license.
  See https://github.com/Pixabay/JavaScript-autoComplete for more details.

- The 'gb.form.htmlview' component embeds the 'litehtml' library from Yuri 
  Kobets. See http://www.litehtml.com for more details.

- The 'gb.hash' component embeds the code of the hashing routines of BusyBox made
  by Denys Vlasenko and Bernhard Reutner-Fischer. See https://www.busybox.net/
  for more details.

- The stock icons flags were generated from SVG files taken from
  https://github.com/smucode/react-world-flags. They are published under the MIT
  license.
  
- The algorithm of the [Paint.SmoothPolyline()](https://gambaswiki.org/wiki/comp/gb.qt4/paint/smoothpolyline)
  method is based on https://www.particleincell.com/2012/bezier-splines/ and
  https://www.jacos.nl/jacos_html/spline/circular/index.html.
  
If I forget some borrowed code in the list above, just tell me.

Enjoy Gambas!

--
Benoît
