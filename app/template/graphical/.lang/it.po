#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: graphical 3.8.90\n"
"POT-Creation-Date: 2020-11-18 18:43 UTC\n"
"PO-Revision-Date: 2015-09-18 02:25 UTC\n"
"Last-Translator: Gianluigi Gradaschi <bagonergi@gmail.com>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Graphical application"
msgstr "Applicazione grafica"

#: .project:2
msgid "A graphical application."
msgstr "Un'applicazione grafica"
