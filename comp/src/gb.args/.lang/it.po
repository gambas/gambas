#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.args 3.18.90\n"
"POT-Creation-Date: 2024-02-14 13:15 UTC\n"
"PO-Revision-Date: 2024-02-14 13:15 UTC\n"
"Last-Translator: gian <gian@gian>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# gb-ignore
#: .project:1
msgid "gb.args"
msgstr ""

#: Args.module:85
msgid "Display version"
msgstr "Mostra la versione"

#: Args.module:92
msgid "Display this help"
msgstr "Mostra questo aiuto"

#: Args.module:100
msgid "unknown option: &1"
msgstr "opzione sconosciuta: &1"

#: Args.module:117
msgid "Error: Args.Helptext() must be called after the command Args.Begin()"
msgstr "Errore: Args.Helptext() deve essere chiamato dopo il comando Args.Begin()"

#: Args.module:124
msgid "Usage: &1 <options> <arguments>"
msgstr "Utilizza: &1 <options> <arguments>"

#: Args.module:127
msgid "Options:"
msgstr "Opzioni:"

#: Args.module:163
msgid "by default"
msgstr "per impostazione predefinita"
