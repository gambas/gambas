#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.gui.base 3.20.99\n"
"POT-Creation-Date: 2025-01-06 22:46 UTC\n"
"PO-Revision-Date: 2025-01-06 22:33 UTC\n"
"Last-Translator: gian <gian@gian>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Common controls and classes for GUI components"
msgstr "Controlli e classi comuni per i componenti della GUI"

#: FGridView.form:34 FListBox.form:40 FTestCombo.form:79 FTestGridView.form:36
#: FTestTreeView.form:58 FTreeView.form:57
msgid "Border"
msgstr "Bordo"

#: FGridView.form:41 FTestGridView.form:43
msgid "Drag me"
msgstr "Trascinami"

#: FGridView.form:46 FTestGridView.form:48
msgid "Selection"
msgstr "Selezione"

# gb-ignore
#: FGridView.form:69 FTestCombo.form:153 FTestGridView.form:75
msgid "TextBox1"
msgstr ""

# gb-ignore
#: FGridView.form:76 FTestGridView.form:82
msgid "TextArea1"
msgstr ""

#: FLabel.form:23
msgid "     This is a Label test line"
msgstr "      Questa è una linea di test della Label"

#: FLabel.form:31 FTestLabel.form:92
msgid "This is a Label test line"
msgstr "Questa è una linea di test della Label"

#: FLabel.form:39 FTestLabel.form:35
msgid ""
"This is a Label test line\n"
"with another one"
msgstr ""
"Questa è una linea di test della Label↵\n"
"con un'altra linea"

#: FLabel.form:55 FTestLabel.form:57
msgid "This is a TextLabel test line"
msgstr "Questa è una linea di test della TextLabel"

#: FListBox.form:22
msgid "Antoine"
msgstr ""

#: FListBox.form:22
msgid "Arnaud"
msgstr ""

#: FListBox.form:22
msgid "B"
msgstr ""

#: FListBox.form:22
msgid "Benedicte"
msgstr ""

#: FListBox.form:22
msgid "Bernard"
msgstr ""

#: FListBox.form:22
msgid "C"
msgstr ""

#: FListBox.form:22
msgid "Charles"
msgstr ""

#: FListBox.form:22
msgid "Dimitri"
msgstr ""

#: FListBox.form:22
msgid "Fabien"
msgstr ""

#: FListBox.form:22
msgid "Farid"
msgstr ""

#: FListBox.form:22
msgid "Marthe"
msgstr ""

#: FListBox.form:22
msgid "Mathilde"
msgstr ""

#: FListBox.form:22
msgid "Pierre"
msgstr ""

#: FListBox.form:22
msgid "a"
msgstr ""

#: FListBox.form:22
msgid "שלום וברכה"
msgstr ""

#: FListBox.form:35
msgid "Add"
msgstr ""

#: FListBox.form:53
msgid "Wrap"
msgstr ""

# gb-ignore
#: FMessage.class:60 Message.class:31
msgid "OK"
msgstr ""

#: FMessage.form:33
msgid "Copy message"
msgstr "Copia messaggio"

#: FRightToLeft.form:23
msgid ""
"Change Language To:\n"
"System.Language = \"he_IL\""
msgstr ""

#: FRightToLeft.form:28
msgid ""
"Change Language To:\n"
"System.Language = \"us_EN\""
msgstr ""

#: FRightToLeft.form:33
msgid "ABC"
msgstr ""

#: FRightToLeft.form:33
msgid "ENGLISH"
msgstr ""

#: FRightToLeft.form:39
msgid "אבג"
msgstr ""

#: FRightToLeft.form:39
msgid "עברית"
msgstr ""

#: FRightToLeft.form:45
msgid "Direction=LeftToRight"
msgstr ""

#: FRightToLeft.form:51
msgid "Direction=RightToLeft"
msgstr ""

#: FRightToLeft.form:77
msgid "עברית (Good)"
msgstr ""

#: FRightToLeft.form:83
msgid "ENGLISH (Good)"
msgstr ""

#: FScrollArea.form:77
msgid "test"
msgstr ""

#: FScrollArea.form:95
msgid "Tab 1"
msgstr ""

#: FScrollArea.form:97
msgid "Tab 2"
msgstr ""

#: FScrollArea.form:356
msgid "Button"
msgstr ""

# gb-ignore
#: FTestCombo.form:35 FTestTreeView.form:28 FTreeView.form:27
msgid "Menu1"
msgstr ""

# gb-ignore
#: FTestCombo.form:39 FTestTreeView.form:32 FTreeView.form:31
msgid "Menu2"
msgstr ""

# gb-ignore
#: FTestCombo.form:43 FTestTreeView.form:36 FTreeView.form:35
msgid "Menu3"
msgstr ""

#: FTestCombo.form:53 FTestProgressBar.form:28
msgid "Button1"
msgstr ""

#: FTestCombo.form:59
msgid "2"
msgstr ""

#: FTestCombo.form:64
msgid "Index"
msgstr ""

#: FTestCombo.form:69
#, fuzzy
msgid "Message"
msgstr "Copia messaggio"

#: FTestCombo.form:74
msgid "Read-only"
msgstr ""

#: FTestCombo.form:86
msgid "bxvcbxcbc"
msgstr ""

#: FTestCombo.form:92
msgid "Élément 1"
msgstr ""

#: FTestCombo.form:92
msgid "Élément 2"
msgstr ""

#: FTestCombo.form:92
msgid "Élément 3"
msgstr ""

#: FTestCombo.form:92
msgid "Élément 4"
msgstr ""

#: FTestCombo.form:92
msgid "Élément 5"
msgstr ""

#: FTestCombo.form:98
msgid "dhdgfhdfghdf"
msgstr ""

#: FTestCombo.form:105
msgid "Frame"
msgstr ""

#: FTestCombo.form:109
msgid "tyrtyrty"
msgstr ""

#: FTestCombo.form:120
msgid "trytryertyter"
msgstr ""

#: FTestCombo.form:133
msgid "fdsgsdg"
msgstr ""

#: FTestCombo.form:148
msgid "1"
msgstr ""

#: FTestCombo.form:158
msgid "Normal quoi"
msgstr ""

#: FTestCombo.form:163
msgid "3"
msgstr ""

#: FTestFileView.form:22
msgid "Toggle view"
msgstr ""

#: FTestFrame.form:17
msgid "gfdhgdfhfdg"
msgstr ""

#: FTestGridView.form:53
msgid "Height"
msgstr ""

#: FTestGridViewRtl.form:20
msgid "Direction.RightToLeft"
msgstr ""

#: FTestGridViewRtl.form:25
msgid "Test"
msgstr ""

#: FTestLabel.form:26
#, fuzzy
msgid "This is a Label test line A"
msgstr "Questa è una linea di test della Label"

#: FTestLabel.form:43
#, fuzzy
msgid "This is a Label test lineT"
msgstr "Questa è una linea di test della Label"

#: FTestLabel.form:50
msgid "\"\""
msgstr ""

#: FTestLabel.form:65
#, fuzzy
msgid "line This is a TextLabel test line with another One"
msgstr ""
"Questa è una linea di test della Label↵\n"
"con un'altra linea"

#: FTestLabel.form:99
msgid "Nom du paquet"
msgstr ""

#: FTestLabel2.form:18
msgid "dev-bm-physidia-contrat"
msgstr ""

#: FTestListView.form:11
msgid "ListView Bug Demo"
msgstr ""

#: FTestListView.form:20
msgid "Delete Selected Item"
msgstr ""

#: FTestListView.form:25
msgid "Fill List"
msgstr ""

#: FTestListView.form:30
msgid "Multiselect Mode"
msgstr ""

#: FTestMouseWheel.form:31
msgid ""
"vfdg\n"
"df\n"
"gd\n"
"fsgsdfgsdf\n"
"gd\n"
"fg\n"
"dsfg\n"
"dsf\n"
"gs\n"
"dfg\n"
"sdf\n"
"g\n"
"sdf"
msgstr ""

#: FTestMouseWheel.form:47
msgid "ok"
msgstr ""

#: FTestSpinBox.form:48
msgid "Disable"
msgstr ""

#: FTestSpinBox.form:53
msgid "Read only"
msgstr ""

# gb-ignore
#: FTestTreeView.form:40 FTreeView.form:39
msgid "Menu4"
msgstr ""

# gb-ignore
#: FTestTreeView.form:44 FTreeView.form:43
msgid "Menu5"
msgstr ""

#: FTestTreeView.form:53 FTreeView.form:52 Message.class:55
msgid "Delete"
msgstr "Elimina"

#: FTestTreeView.form:63 FTreeView.form:62
msgid "Drag"
msgstr "Trascina"

# gb-ignore
#: FTestTreeView.form:68 FTreeView.form:67
msgid "Reparent"
msgstr ""

#: FTestTreeView.form:73 FTreeView.form:72
msgid "Rename"
msgstr "Rinomina"

#: FTestTreeView.form:78 FTreeView.form:77
msgid "Check"
msgstr "Controlla"

#: FTestTreeView.form:83
msgid "Enabled"
msgstr ""

#: FTestTreeView.form:88
msgid "Hide"
msgstr ""

#: FileView.class:131
msgid "Name"
msgstr ""

#: FileView.class:133
msgid "Size"
msgstr ""

#: FileView.class:135
msgid "Last modified"
msgstr ""

#: FileView.class:1062
msgid "Cannot rename file."
msgstr ""

#: Help.module:71
msgid "A file or directory name cannot be void."
msgstr ""

#: Help.module:72
msgid "The '/' character is forbidden inside file or directory names."
msgstr ""

# gb-ignore
#: Main.module:69
msgid "&1 B"
msgstr ""

# gb-ignore
#: Main.module:71
msgid "&1 KiB"
msgstr ""

# gb-ignore
#: Main.module:73
msgid "&1 MiB"
msgstr ""

# gb-ignore
#: Main.module:75
msgid "&1 GiB"
msgstr ""

#: Message.class:43
msgid "Cancel"
msgstr "Annulla"

#: Message.class:43
msgid "Yes"
msgstr "Sì"
